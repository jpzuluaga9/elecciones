# Dapp de votaciones para ethereum



### Requisitos:
* Nodejs.
* Trufflejs.
* web3js.
* ganache-cli.

### Instalar:
$ ./scripts/INSTALL.sh
$ npm install
### Ejecutar:
$ ./scripts/serve.sh
$ ./scripts/run-dev.sh

